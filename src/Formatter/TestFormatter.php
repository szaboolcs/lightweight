<?php

namespace LightWeight\Framework\Formatter;

use LightWeight\Framework\DataService\Report\Test\Domain\Aggregate\ChartSummary;
use LightWeight\Framework\DataService\Report\Test\Domain\Aggregate\LongForm;
use LightWeight\Framework\DataService\Report\Test\Domain\Aggregate\MainRegistration;

class TestFormatter
{
    /**
     * @param ChartSummary $chartSummary
     *
     * @return array
     */
    public function formatGetChartSummaryData(\DateTime $dateFrom, \DateTime $dateTo, ChartSummary $chartSummary): array
    {
        $array     = [];
        $dateRange = $this->getDateRange($dateFrom, $dateTo);

        foreach ($chartSummary->getOpened() as $opened) {
            $array['opened'][$opened['casted_date']][$opened['channel']] = $opened['count'];
            $array['opened'][$opened['casted_date']]['date']             = $opened['casted_date'];
        }

        foreach ($chartSummary->getLost() as $lost) {
            $array['lost'][$lost['casted_date']][$lost['channel']] = $lost['count'];
            $array['lost'][$lost['casted_date']]['date']           = $lost['casted_date'];
        }

        foreach ($chartSummary->getLostProcessSide() as $lostProcessSide) {
            $array['lostProcessSide'][$lostProcessSide['casted_date']][$lostProcessSide['channel']] = $lostProcessSide['count'];
            $array['lostProcessSide'][$lostProcessSide['casted_date']]['date']                      = $lostProcessSide['casted_date'];
        }

        foreach ($chartSummary->getLostUserSide() as $lostUserSide) {
            $array['lostUserSide'][$lostUserSide['casted_date']][$lostUserSide['channel']] = $lostUserSide['count'];
            $array['lostUserSide'][$lostUserSide['casted_date']]['date']                   = $lostUserSide['casted_date'];
        }

        foreach ($chartSummary->getSuccessful() as $successful) {
            $array['successful'][$successful['casted_date']][$successful['channel']] = $successful['count'];
            $array['successful'][$successful['casted_date']]['date']                 = $successful['casted_date'];
        }

        foreach ($array as $type => $data) {
            $this->fillEmptyDates($dateRange, $data);

            foreach ($data as $date => $value) {
                if (!isset($value['web'])) {
                    $array[$type][$date]['web'] = 0;
                }

                if (!isset($value['mobile'])) {
                    $array[$type][$date]['mobile'] = 0;
                }
            }
        }

        foreach ($array as $type => $data) {
            $array[$type] = array_values($data);
        }

        return $array;
    }

    /**
     * @param MainRegistration $mainRegistration
     *
     * @return array
     */
    public function formatGetChartMainRegistrationData(\DateTime $dateFrom, \DateTime $dateTo, MainRegistration $mainRegistration): array
    {
        $array     = [];
        $dateRange = $this->getDateRange($dateFrom, $dateTo);

        foreach ($mainRegistration->getOpened() as $item) {
            $array['opened'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['opened'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getActivatedUsersDailyStatistics() as $item) {
            $array['activatedUsers'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['activatedUsers'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getActivationEmailSentUsersDailyStatistics() as $item) {
            $array['emailSentUsers'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['emailSentUsers'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getClubcardAssignmentAllocatedCardDailyStatistics() as $item) {
            $array['clubcardAssignmentAllocated'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['clubcardAssignmentAllocated'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getClubcardAssignmentOwnCardDailyStatistics() as $item) {
            $array['clubcardAssignmentOwn'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['clubcardAssignmentOwn'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getClubcardAssignmentDailyStatistics() as $item) {
            $array['clubcardAssignment'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['clubcardAssignment'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getFormSubmittedDailyStatistics() as $item) {
            $array['formSubmitted'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['formSubmitted'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getFormSubmittedLongDailyStatistics() as $item) {
            $array['formSubmittedLong'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['formSubmittedLong'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getFormSubmittedShortDailyStatistics() as $item) {
            $array['formSubmittedShort'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['formSubmittedShort'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getMcaActivationSuccessfulDailyStatistics() as $item) {
            $array['mcaActivationSuccessful'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['mcaActivationSuccessful'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getMcaRegisterSuccessfulDailyStatistics() as $item) {
            $array['mcaRegisterSuccessful'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['mcaRegisterSuccessful'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($mainRegistration->getRegistrationSuccessfulDailyStatistics() as $item) {
            $array['registrationSuccessful'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['registrationSuccessful'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($array as $type => $data) {
            $this->fillEmptyDates($dateRange, $data);

            foreach ($data as $date => $value) {
                if (!isset($value['web'])) {
                    $array[$type][$date]['web'] = 0;
                }

                if (!isset($value['mobile'])) {
                    $array[$type][$date]['mobile'] = 0;
                }
            }
        }

        foreach ($array as $type => $data) {
            $array[$type] = array_values($data);
        }

        return $array;
    }

    /**
     * @param LongForm $longForm
     *
     * @return array
     */
    public function formatGetChartLongFormData(\DateTime $dateFrom, \DateTime $dateTo, LongForm $longForm): array
    {
        $array     = [];
        $dateRange = $this->getDateRange($dateFrom, $dateTo);

        foreach ($longForm->getFormSubmittedLongDailyStatistics() as $item) {
            $array['formSubmittedLong'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['formSubmittedLong'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($longForm->getLongFormXmlPackUploaded() as $item) {
            $array['longFormXmlPackUploaded'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['longFormXmlPackUploaded'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($longForm->getLongFormRejectDownload() as $item) {
            $array['longFormRejectDownload'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['longFormRejectDownload'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($longForm->getLongFormXmlPackGenerated() as $item) {
            $array['longFormXmlPackGenerated'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['longFormXmlPackGenerated'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($longForm->getNgcFinishedSuccessful() as $item) {
            $array['ngcFinishedSuccessful'][$item['casted_date']][$item['channel']] = $item['count'];
            $array['ngcFinishedSuccessful'][$item['casted_date']]['date']           = $item['casted_date'];
        }

        foreach ($array as $type => $data) {
            $this->fillEmptyDates($dateRange, $data);

            foreach ($data as $date => $value) {
                if (!isset($value['web'])) {
                    $array[$type][$date]['web'] = 0;
                }

                if (!isset($value['mobile'])) {
                    $array[$type][$date]['mobile'] = 0;
                }
            }
        }

        foreach ($array as $type => $data) {
            $array[$type] = array_values($data);
        }

        return $array;
    }

    /**
     * @param array $dateRange
     * @param array $array
     */
    private function fillEmptyDates(array $dateRange, array &$array)
    {
        foreach ($dateRange as $date) {
            if (!isset($array[$date])) {
                $array[$date] = [
                    'web'    => 0,
                    'date'   => $date,
                    'mobile' => 0,
                ];
            }
        }
    }

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return array
     */
    private function getDateRange(\DateTime $dateFrom, \DateTime $dateTo): array
    {
        $interval = \DateInterval::createFromDateString('1 day');
        $period   = new \DatePeriod($dateFrom, $interval, $dateTo);

        $array = [];

        foreach ($period as $dt) {
            $array[] = $dt->format('Y-m-d');
        }

        return $array;
    }
}
