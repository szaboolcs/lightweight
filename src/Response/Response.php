<?php

namespace LightWeight\Framework\Response;

use LightWeight\Framework\Container\Container;
use LightWeight\Framework\Contract\ResponseInterface;

class Response implements ResponseInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * Response constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $data
     */
    public function withJson(array $data)
    {
        header('Content-type: application/json');

        echo json_encode($data);
    }
}
