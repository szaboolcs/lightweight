<?php
namespace LightWeight\Framework\Controller;

use LightWeight\Database\Collection\ConnectionManager;
use LightWeight\Framework\DataService\Report\Test\Application\Exception\InvalidDatabaseException;
use LightWeight\Framework\DataService\Report\Test\Application\Service\TestService;
use LightWeight\Framework\DataService\Report\Test\Domain\ValueObject\Database;
use LightWeight\Framework\Contract\ConnectionInterface;
use LightWeight\Framework\Formatter\TestFormatter;

class TestController extends AbstractController
{
    /**
     * @param string $dateFrom
     * @param string $dateTo
     * @param string $country
     * @param array $body
     *
     * @return array
     *
     * @throws InvalidDatabaseException
     */
    public function getChartSummaryData(string $dateFrom, string $dateTo, string $country, array $body): array
    {
        /* @var ConnectionManager $connection */
        $connection          = $this->container->get('database');
        $TestService = new TestService($connection->get('default'));

        $dateFrom     = new \DateTime($dateFrom);
        $dateTo       = new \DateTime($dateTo);
        $database     = Database::buildFromCountryCode($country);
        $chartSummary = $TestService->getChartSummaryData($dateFrom, $dateTo, $database);

        $formatter = new TestFormatter();

        return $formatter->formatGetChartSummaryData($dateFrom, $dateTo, $chartSummary);
    }

    /**
     * @param string $dateFrom
     * @param string $dateTo
     * @param string $country
     * @param array $body
     *
     * @return array
     *
     * @throws InvalidDatabaseException
     */
    public function getChartMainRegistrationData(string $dateFrom, string $dateTo, string $country, array $body): array
    {
        /* @var ConnectionManager $connection */
        $connection          = $this->container->get('database');
        $TestService = new TestService($connection->get('default'));

        $dateFrom             = new \DateTime($dateFrom);
        $dateTo               = new \DateTime($dateTo);
        $database             = Database::buildFromCountryCode($country);
        $mainRegistrationData = $TestService->getChartMainRegistrationData($dateFrom, $dateTo, $database);

        $formatter = new TestFormatter();

        return $formatter->formatGetChartMainRegistrationData($dateFrom, $dateTo, $mainRegistrationData);
    }

    /**
     * @param string $dateFrom
     * @param string $dateTo
     * @param string $country
     * @param array $body
     *
     * @return array
     *
     * @throws InvalidDatabaseException
     */
    public function getChartLongFormData(string $dateFrom, string $dateTo, string $country, array $body): array
    {
        /* @var ConnectionManager $connection */
        $connection          = $this->container->get('database');
        $TestService = new TestService($connection->get('default'));

        $dateFrom     = new \DateTime($dateFrom);
        $dateTo       = new \DateTime($dateTo);
        $database      = Database::buildFromCountryCode($country);
        $longFormData = $TestService->getChartLongFormData($dateFrom, $dateTo, $database);

        $formatter = new TestFormatter();

        return $formatter->formatGetChartLongFormData($dateFrom, $dateTo, $longFormData);
    }
}
