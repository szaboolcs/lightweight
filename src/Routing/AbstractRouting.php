<?php

namespace LightWeight\Framework\Routing;

abstract class AbstractRouting
{
    const ROUTE_KEY     = 'route';
    const CLASS_KEY     = 'class';
    const ACTION_KEY    = 'action';
    const PROTECTED_KEY = 'protected';

    /**
     * @return array
     */
    abstract public function get(): array;
}
