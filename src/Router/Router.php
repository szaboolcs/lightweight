<?php
namespace LightWeight\Framework\Router;

use LightWeight\Framework\Container\Container;
use LightWeight\Framework\Contract\RequestInterface;
use LightWeight\Framework\Contract\RouterInterface;
use LightWeight\Framework\Contract\RoutingInterface;
use LightWeight\Framework\Exception\InvalidActionException;
use LightWeight\Framework\Exception\InvalidControllerException;
use LightWeight\Framework\Exception\InvalidRequestMethodException;
use LightWeight\Framework\Exception\RouteNotFoundException;
use LightWeight\Framework\Routing\AbstractRouting;

class Router implements RouterInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var string[]
     */
    private $supportedHttpMethods = ['get', 'post'];

    /**
     * Router constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     *
     * @throws InvalidActionException
     * @throws InvalidControllerException
     * @throws InvalidRequestMethodException
     * @throws RouteNotFoundException
     */
    public function resolve()
    {
        $request = $this->container->get(RequestInterface::class);

        if (!in_array($request->getRequestMethod(), $this->supportedHttpMethods)) {
            throw new InvalidRequestMethodException('Invalid method.');
        }

        $routing = $this->container->get(RoutingInterface::class);
        $uri     = $request->getUri();

        foreach ($routing->get() as $route) {
            $regex = '/^' . preg_replace(['/{\w+}/', '/\//'], ['[A-Za-z0-9_-]+', '\/'], $route[AbstractRouting::ROUTE_KEY]) . '$/';

            if (preg_match($regex, $uri)) {
                return $this->handleRoute($uri, $route);
            }
        }

        throw new RouteNotFoundException('Route not found.');
    }

    /**
     * @param string $uri
     * @param string $route
     *
     * @return mixed
     *
     * @throws InvalidActionException
     * @throws InvalidControllerException
     */
    private function handleRoute(string $uri, array $route)
    {
        $request         = $this->container->get(RequestInterface::class);
        $uriParameters   = explode('/', $uri);
        $routeParameters = explode('/', $route[AbstractRouting::ROUTE_KEY]);
        $items           = array_map(function($item, $key) use ($uriParameters) {
            return preg_match('/{\w+}/', $item) ? $uriParameters[$key] : null;
        }, $routeParameters, array_keys($routeParameters));

        if (!class_exists($route[AbstractRouting::CLASS_KEY])) {
            throw new InvalidControllerException('Controller does not exists: ' . $route[AbstractRouting::CLASS_KEY]);
        }

        $parameters = array_filter($items);
        $controller = new $route[AbstractRouting::CLASS_KEY]($this->container);

        if (!method_exists($controller, $route[AbstractRouting::ACTION_KEY])) {
            throw new InvalidActionException('Action does not exists: ' . $route[AbstractRouting::ACTION_KEY]);
        }

        $parameters[] = $request->getBody();

        return call_user_func_array([
            $controller,
            $route[AbstractRouting::ACTION_KEY]
        ], $parameters);
    }
}
