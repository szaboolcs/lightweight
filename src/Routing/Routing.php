<?php

namespace LightWeight\Framework\Routing;

use LightWeight\Framework\Container\Container;

class Routing
{
    /**
     * @var Container
     */
    private $container;

    /**
     * Routing constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function get()
    {
        return array_merge(
            (new TestRouting())->get()
        );
    }
}
