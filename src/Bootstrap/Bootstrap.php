<?php

namespace LightWeight\Framework\Bootstrap;

use LightWeight\Database\Adapter\MysqliAdapter;
use LightWeight\Database\Adapter\MysqliNameBindingResolver;
use LightWeight\Database\Collection\ConnectionManager;
use LightWeight\Database\Config\ConnectionConfig;
use LightWeight\Database\Logger\Adapter\PhpArrayAdapter;
use LightWeight\Database\Logger\QueryLogger;
use LightWeight\Database\Service\ConnectionService;
use LightWeight\Framework\Container\Container;
use LightWeight\Framework\Contract\RequestInterface;
use LightWeight\Framework\Contract\ResponseInterface;
use LightWeight\Framework\Contract\RouterInterface;
use LightWeight\Framework\Contract\RoutingInterface;
use LightWeight\Framework\Request\Request;
use LightWeight\Framework\Response\Response;
use LightWeight\Framework\Router\Router;
use LightWeight\Framework\Routing\Routing;

class Bootstrap
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $databaseSettings;

    /**
     * Bootstrap constructor.
     *
     * @param Container $container
     * @param array $databaseSettings
     */
    public function __construct(Container $container, array $databaseSettings)
    {
        $this->container        = $container;
        $this->databaseSettings = $databaseSettings;
    }

    public function initialize()
    {
        $this->initConnection();
        $this->initRoutes();
        $this->initRouter();
        $this->initRequest();
        $this->initResponse();
    }

    private function initConnection()
    {
        $connectionManager = new ConnectionManager();
        $queryLogAdapter   = new PhpArrayAdapter();
        $queryLogger       = new QueryLogger($queryLogAdapter);
        $resolver          = new MysqliNameBindingResolver();

        foreach ($this->databaseSettings as $connectionName => $connectionSettings) {
            $connectionConfig = new ConnectionConfig(
                $connectionSettings['database'],
                $connectionSettings['username'],
                $connectionSettings['password']
            );
            $connectionConfig->setHostname($connectionSettings['hostname']);

            $adapter    = new MysqliAdapter($connectionConfig, $resolver);
            $connection = new ConnectionService($adapter, $queryLogger);

            $connectionManager->add($connectionName, $connection);
        }

        $this->container->add(
            'database',
            $connectionManager
        );
    }

    private function initRoutes()
    {
        $this->container->add(
            RoutingInterface::class,
            new Routing($this->container)
        );
    }

    private function initRouter()
    {
        $this->container->add(
            RouterInterface::class,
            new Router($this->container)
        );
    }

    private function initRequest()
    {
        $this->container->add(
            RequestInterface::class,
            new Request($this->container)
        );
    }

    private function initResponse()
    {
        $this->container->add(
            ResponseInterface::class,
            new Response($this->container)
        );
    }
}
