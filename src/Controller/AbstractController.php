<?php

namespace LightWeight\Framework\Controller;

use LightWeight\Framework\Container\Container;

abstract class AbstractController
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * AbstractController constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    protected function run()
    {

    }
}
