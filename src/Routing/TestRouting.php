<?php

namespace LightWeight\Framework\Routing;

use LightWeight\Framework\Controller\TestController;

class TestRouting extends AbstractRouting
{
    /**
     * @return array|\string[][]
     */
    public function get(): array
    {
        return [
            [
                self::ROUTE_KEY     => 'test/get-chart-summary-data/{dateFrom}/{dateTo}/{country}',
                self::CLASS_KEY     => TestController::class,
                self::ACTION_KEY    => 'getChartSummaryData',
                self::PROTECTED_KEY => true,
            ],
            [
                self::ROUTE_KEY     => 'test/get-chart-main-registration-data/{dateFrom}/{dateTo}/{country}',
                self::CLASS_KEY     => TestController::class,
                self::ACTION_KEY    => 'getChartMainRegistrationData',
                self::PROTECTED_KEY => true,
            ],
            [
                self::ROUTE_KEY     => 'test/get-chart-long-form-data/{dateFrom}/{dateTo}/{country}',
                self::CLASS_KEY     => TestController::class,
                self::ACTION_KEY    => 'getChartLongFormData',
                self::PROTECTED_KEY => true,
            ]
        ];
    }
}
