<?php

namespace LightWeight\Framework\Request;

use LightWeight\Framework\Container\Container;
use LightWeight\Framework\Contract\RequestInterface;

class Request implements RequestInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $data;

    /**
     * Request constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->data      = $_SERVER;
    }

    /**
     * @return string
     */
    public function getRequestMethod()
    {
        return strtolower($this->data['REQUEST_METHOD']);
    }

    /**
     * @return bool
     */
    public function isGetRequest()
    {
        return $this->getRequestMethod() === 'get';
    }

    /**
     * @return bool
     */
    public function isPostRequest()
    {
        return $this->getRequestMethod() === 'post';
    }

    /**
     * @return string
     */
    public function getUri()
    {
        $array = explode('/', trim($this->data['REQUEST_URI'], '/'));

        array_shift($array);

        return implode('/', $array);
    }

    /**
     * @return array
     */
    public function getBody()
    {
        $body = [];

        if ($this->isGetRequest()) {
            return $body;
        }

        foreach($_POST as $key => $value) {
            $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
        }

        return $body;
    }
}
