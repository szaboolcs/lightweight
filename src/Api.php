<?php

namespace LightWeight\Framework;

use LightWeight\Framework\Bootstrap\Bootstrap;
use LightWeight\Framework\Container\Container;
use LightWeight\Framework\Contract\ResponseInterface;
use LightWeight\Framework\Contract\RouterInterface;

class Api
{
    /**
     * @param array $databaseSettings
     */
    public function run(array $databaseSettings)
    {
        $container = new Container();
        $bootstrap = new Bootstrap($container, $databaseSettings);

        $bootstrap->initialize();

        $router   = $container->get(RouterInterface::class);
        $response = $container->get(ResponseInterface::class);

        try {
            $data = $router->resolve();

            $response->withJson($data);
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }

        exit;
    }
}
